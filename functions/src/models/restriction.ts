export class Restriction {
    date: Date;
    dayCondition: string;
    nightCondition: string;
    dayPlates: number[];
    nightPlates: number[];
    startDayTime: Date;
    endDayTime: Date;
    startNightTime: Date;
    endNightTime: Date;
    noSchedule: boolean;
}
