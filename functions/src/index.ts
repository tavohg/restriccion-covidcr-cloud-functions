import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { PlateSubscriber } from './models/plateSubscriber';
import * as moment from 'moment';
// tslint:disable-next-line: no-import-side-effect
import 'moment/locale/es-do';

admin.initializeApp();
const db = admin.firestore();

// Sendgrid Config
import * as sgMail from '@sendgrid/mail';
import { Restriction } from './models/restriction';

const API_KEY = functions.config().sendgrid.key;
const TEMPLATE_ID = functions.config().sendgrid.template;
sgMail.setApiKey(API_KEY);

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

export const dailyNotifications = functions.pubsub
  .schedule('every day 23:05')
  .onRun(async (context) => {
    const today = new Date();
    today.setHours(6, 0, 0, 0);
    const subscribersSnapshots = await admin
      .firestore()
      .collection('subscribers')
      .get();
    const restrictionSnapshot = await admin
      .firestore()
      .collection('restrictions')
      .get();

    const plateSubscribers: PlateSubscriber[] = subscribersSnapshots.docs.map(
      (plateSubscriberDoc) => plateSubscriberDoc.data() as PlateSubscriber
    );

    const todayRestrictionArray: Restriction[] = [];
    await asyncForEach(
      restrictionSnapshot.docs,
      async (restrictionDoc: any) => {
        if (
          restrictionDoc.data().date.toDate().toString() === today.toString()
        ) {
          const todayRestriction = new Restriction();
          todayRestriction.date = await restrictionDoc.data().date.toDate();
          todayRestriction.date.setHours(todayRestriction.date.getHours() - 6);
          todayRestriction.noSchedule = await restrictionDoc.data().noSchedule;
          if (restrictionDoc.data().dayCondition) {
            todayRestriction.dayCondition = await restrictionDoc.data()
              .dayCondition;
          }
          if (restrictionDoc.data().nightCondition) {
            todayRestriction.nightCondition = await restrictionDoc.data()
              .nightCondition;
          }
          todayRestriction.dayPlates = await restrictionDoc.data().dayPlates;
          if (restrictionDoc.data().nightPlates) {
            todayRestriction.nightPlates = await restrictionDoc.data()
              .nightPlates;
          }
          todayRestriction.startDayTime = await restrictionDoc
            .data()
            .startDayTime.toDate();
          todayRestriction.startDayTime.setHours(
            todayRestriction.startDayTime.getHours() - 6
          );
          todayRestriction.endDayTime = await restrictionDoc
            .data()
            .endDayTime.toDate();
          todayRestriction.endDayTime.setHours(
            todayRestriction.endDayTime.getHours() - 6
          );
          if (restrictionDoc.data().startNightTime) {
            todayRestriction.startNightTime = await restrictionDoc
              .data()
              .startNightTime.toDate();
            todayRestriction.startNightTime.setHours(
              todayRestriction.startNightTime.getHours() - 6
            );
          }
          if (restrictionDoc.data().endNightTime) {
            todayRestriction.endNightTime = await restrictionDoc
              .data()
              .endNightTime.toDate();
            todayRestriction.endNightTime.setHours(
              todayRestriction.endNightTime.getHours() - 6
            );
          }
          todayRestrictionArray.push(todayRestriction);
        }
      }
    );

    plateSubscribers.forEach(async (plateSubscriber: PlateSubscriber) => {
      const messages = await setRestriction(
        todayRestrictionArray[0],
        plateSubscriber.plate
      );
      const msg = {
        to: plateSubscriber.email,
        from: 'morpho.techco@gmail.com',
        templateId: TEMPLATE_ID,
        dynamic_template_data: {
          subject: 'Notificación de restricción vehicular sanitaria',
          plate: plateSubscriber.plate,
          messageDay: messages[0] ? 'Durante el día: ' + messages[0] : '',
          messageNight: messages[1] ? 'Durante la noche: ' + messages[1] : '',
        },
      };
      const sended = await sgMail.send(msg);
    });

    return;
  });

// Sends email via HTTP. Can be called from frontend code.
export const manualNotifications = functions.https.onRequest(async (req, res) => {
  const today = new Date();
  today.setHours(6, 0, 0, 0);
  const subscribersSnapshots = await admin
    .firestore()
    .collection('subscribers')
    .get();
  const restrictionSnapshot = await admin
    .firestore()
    .collection('restrictions')
    .get();

  const plateSubscribers: PlateSubscriber[] = subscribersSnapshots.docs.map(
    (plateSubscriberDoc) => plateSubscriberDoc.data() as PlateSubscriber
  );

  const todayRestrictionArray: Restriction[] = [];
  await asyncForEach(restrictionSnapshot.docs, async (restrictionDoc: any) => {
    if (restrictionDoc.data().date.toDate().toString() === today.toString()) {
      const todayRestriction = new Restriction();
      todayRestriction.date = await restrictionDoc.data().date.toDate();
      todayRestriction.date.setHours(todayRestriction.date.getHours() - 6);
      todayRestriction.noSchedule = await restrictionDoc.data().noSchedule;
      if (restrictionDoc.data().dayCondition) {
        todayRestriction.dayCondition = await restrictionDoc.data()
          .dayCondition;
      }
      if (restrictionDoc.data().nightCondition) {
        todayRestriction.nightCondition = await restrictionDoc.data()
          .nightCondition;
      }
      todayRestriction.dayPlates = await restrictionDoc.data().dayPlates;
      if (restrictionDoc.data().nightPlates) {
        todayRestriction.nightPlates = await restrictionDoc.data().nightPlates;
      }
      todayRestriction.startDayTime = await restrictionDoc
        .data()
        .startDayTime.toDate();
      todayRestriction.startDayTime.setHours(
        todayRestriction.startDayTime.getHours() - 6
      );
      todayRestriction.endDayTime = await restrictionDoc
        .data()
        .endDayTime.toDate();
      todayRestriction.endDayTime.setHours(
        todayRestriction.endDayTime.getHours() - 6
      );
      if (restrictionDoc.data().startNightTime) {
        todayRestriction.startNightTime = await restrictionDoc
          .data()
          .startNightTime.toDate();
        todayRestriction.startNightTime.setHours(
          todayRestriction.startNightTime.getHours() - 6
        );
      }
      if (restrictionDoc.data().endNightTime) {
        todayRestriction.endNightTime = await restrictionDoc
          .data()
          .endNightTime.toDate();
        todayRestriction.endNightTime.setHours(
          todayRestriction.endNightTime.getHours() - 6
        );
      }
      todayRestrictionArray.push(todayRestriction);
    }
  });

  plateSubscribers.forEach(async (plateSubscriber: PlateSubscriber) => {
    const messages = await setRestriction(
      todayRestrictionArray[0],
      plateSubscriber.plate
    );
    const msg = {
      to: plateSubscriber.email,
      from: 'morpho.techco@gmail.com',
      templateId: TEMPLATE_ID,
      dynamic_template_data: {
        subject: 'Notificación de restricción vehicular sanitaria',
        plate: plateSubscriber.plate,
        messageDay: messages[0] ? 'Durante el día: ' + messages[0] : '',
        messageNight: messages[1] ? 'Durante la noche: ' + messages[1] : '',
      },
    };
    const sended = await sgMail.send(msg);
  });

  res.end();
});

async function setRestriction(
  consultedRestriction: Restriction,
  plate: string
): Promise<string[]> {
  const messages: string[] = [];
  const lastPlateDigit = parseInt(plate.substring(plate.length - 1), 10);
  let noSchedule: boolean;

  let dayTimeRestriction = false;
  let nightTimeRestriction = false;

  let canTravelDay: boolean;
  let canTravelNight: boolean;

  let dayRestrictionMessage: string;
  let noDayRestrictionMessage: string;
  let nightRestrictionMessage: string;
  let noNightRestrictionMessage: string;

  const momentStartDayTime = moment(
    consultedRestriction.startDayTime.toISOString()
  );

  const momentEndDayTime = moment(
    consultedRestriction.endDayTime.toISOString()
  );

  const momentStartNightTime = consultedRestriction.startNightTime
    ? moment(consultedRestriction.startNightTime.toISOString())
    : null;
  const momentEndNightTime = consultedRestriction.endNightTime
    ? moment(consultedRestriction.endNightTime.toISOString())
    : null;

  if (consultedRestriction.noSchedule) {
    noSchedule = true;
    if (consultedRestriction.dayPlates.length > 0) {
      dayTimeRestriction = true;
      if (consultedRestriction.dayPlates[0] === 10) {
        canTravelDay = false;
      } else {
        canTravelDay = !consultedRestriction.dayPlates.includes(lastPlateDigit);
      }
      canTravelDay
        ? (dayRestrictionMessage =
            'Su vehículo SÍ puede circular durante todo el día ' +
            (consultedRestriction.dayCondition
              ? consultedRestriction.dayCondition
              : '') +
            '.')
        : (dayRestrictionMessage =
            'Su vehículo NO puede circular en este día.');
      messages.push(dayRestrictionMessage);
    }
  } else {
    noSchedule = false;
    if (consultedRestriction.dayPlates.length > 0) {
      dayTimeRestriction = true;
      if (consultedRestriction.dayPlates[0] === 10) {
        canTravelDay = false;
      } else {
        canTravelDay = !consultedRestriction.dayPlates.includes(lastPlateDigit);
      }
      canTravelDay
        ? (dayRestrictionMessage =
            'Su vehículo SÍ puede circular ' +
            (consultedRestriction.dayCondition
              ? consultedRestriction.dayCondition
              : '') +
            ' desde ' +
            momentStartDayTime.locale('es-do').format('LT') +
            ' del ' +
            momentStartDayTime.locale('es-do').format('LL') +
            ' hasta ' +
            momentEndDayTime.locale('es-do').format('LT') +
            ' del ' +
            momentEndDayTime.locale('es-do').format('LL') +
            '.')
        : (dayRestrictionMessage =
            'Su vehículo NO puede circular desde ' +
            momentStartDayTime.locale('es-do').format('LT') +
            ' del ' +
            momentStartDayTime.locale('es-do').format('LL') +
            ' hasta ' +
            momentEndDayTime.locale('es-do').format('LT') +
            ' del ' +
            momentEndDayTime.locale('es-do').format('LL') +
            '.');
      messages.push(dayRestrictionMessage);
    } else {
      dayTimeRestriction = false;
      noDayRestrictionMessage =
        'Su vehículo SÍ puede circular desde ' +
        momentStartDayTime.locale('es-do').format('LT') +
        ' del ' +
        momentStartDayTime.locale('es-do').format('LL') +
        ' hasta ' +
        momentEndDayTime.locale('es-do').format('LT') +
        ' del ' +
        momentEndDayTime.locale('es-do').format('LL') +
        '.';
      messages.push(noDayRestrictionMessage);
    }
    if (consultedRestriction.nightPlates.length > 0) {
      nightTimeRestriction = true;
      if (consultedRestriction.nightPlates[0] === 10) {
        canTravelNight = false;
      } else {
        canTravelNight = !consultedRestriction.nightPlates.includes(
          lastPlateDigit
        );
      }
      canTravelNight
        ? (nightRestrictionMessage =
            'Su vehículo SÍ puede circular ' +
            (consultedRestriction.nightCondition
              ? consultedRestriction.nightCondition
              : '') +
            ' desde ' +
            momentStartNightTime.locale('es-do').format('LT') +
            ' del ' +
            momentStartNightTime.locale('es-do').format('LL') +
            ' hasta ' +
            momentEndNightTime.locale('es-do').format('LT') +
            ' del ' +
            momentEndNightTime.locale('es-do').format('LL') +
            '.')
        : (nightRestrictionMessage =
            'Su vehículo NO puede circular desde ' +
            momentStartNightTime.locale('es-do').format('LT') +
            ' del ' +
            momentStartNightTime.locale('es-do').format('LL') +
            ' hasta ' +
            momentEndNightTime.locale('es-do').format('LT') +
            ' del ' +
            momentEndNightTime.locale('es-do').format('LL') +
            '.');
      messages.push(nightRestrictionMessage);
    } else {
      nightTimeRestriction = false;
      noNightRestrictionMessage =
        'Su vehículo SÍ puede circular desde ' +
        momentStartNightTime.locale('es-do').format('LT') +
        ' del ' +
        momentStartNightTime.locale('es-do').format('LL') +
        ' hasta ' +
        momentEndNightTime.locale('es-do').format('LT') +
        ' del ' +
        momentEndNightTime.locale('es-do').format('LL') +
        '.';
      messages.push(noNightRestrictionMessage);
    }
  }
  return messages;
}

/**
 * This is a function that simulates an asynchronus forEach function.
 */
async function asyncForEach(array: any, callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
